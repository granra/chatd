#define OP_SERV 1 /* Message from server            */
#define OP_ERR  2 /* Error message                  */
#define OP_WHO  3 /* /who command from client       */
#define OP_LIST 4 /* /list command from client      */
#define OP_JOIN 5 /* /join command from client      */
#define OP_MSG  6 /* A text message from the client */
#define OP_PRVT 7 /* Private message from client    */
#define OP_AUTH 8 /* Authentication                 */

#define MSG_SIZE        512
#define SIMPLE_BUF_SIZE 510
#define RES_BUF_SIZE    508

typedef struct packet_simple {
	short opcode;
	char  buf[SIMPLE_BUF_SIZE];
} packet_simple;

typedef struct packet_res {
	short opcode;
	short success;
	char  buf[RES_BUF_SIZE];
} packet_res;

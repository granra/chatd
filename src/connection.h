typedef struct connection {
    int sockfd;
    SSL *ssl;
    char username[60];
    char *chatroom;
    int idle;
    int auth_tries;
} connection;

connection* connection_new(SSL *ssl, int sockfd) {
    connection *conn = g_new0(connection, 1);
    conn->sockfd = sockfd;
    conn->ssl = ssl;
    /* Initially user doesn't belong to a chatroom */
    conn->chatroom = NULL;
    conn->idle = 0;
    conn->auth_tries = 0;
    return conn;
}

void connection_free(gpointer data) {
    SSL_shutdown(((connection*)data)->ssl);
    SSL_free(((connection*)data)->ssl);
    close(((connection*)data)->sockfd);
    free(data);
}

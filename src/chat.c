/* A UDP echo server with timeouts.
 *
 * Note that you will not need to use select and the timeout for a
 * tftp server. However, select is also useful if you want to receive
 * from multiple sockets at the same time. Read the documentation for
 * select on how to do this (Hint: Iterate with FD_ISSET()).
 */

#include <assert.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <signal.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <glib.h>

/* Secure socket layer headers */
#include <openssl/ssl.h>
#include <openssl/err.h>

/* For nicer interaction, we use the GNU readline library. */
#include <readline/readline.h>
#include <readline/history.h>

#include "packet_types.h"
#include "sha256.h"
#include "fullpath.h"

#define RETURN_NULL(x) if ((x)==NULL) exit (1)
#define RETURN_ERR(err,s) if ((err)==-1) { perror(s); exit(1); }
#define RETURN_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); exit(1); }

/* To suppress unused parameter warning */
#define UNUSED(x) (void)(x)

/* This variable is 1 while the client is active and becomes 0 after
   a quit command to terminate the client and to clean up the
   connection. */
static int active = 1;

char salt[] = "bv5PehSMfV11Cd";
#define salt_length 14

/* To read a password without echoing it to the console.
 *
 * We assume that stdin is not redirected to a pipe and we won't
 * access tty directly. It does not make much sense for this program
 * to redirect input and output.
 *
 * This function is not safe to termination. If the program
 * crashes during getpasswd or gets terminated, then echoing
 * may remain disabled for the shell (that depends on shell,
 * operating system and C library). To restore echoing,
 * type 'reset' into the sell and press enter.
 */
void getpasswd(const char *prompt, char *passwd, size_t size)
{
	struct termios old_flags, new_flags;

        /* Clear out the buffer content. */
        memset(passwd, 0, size);

        /* Disable echo. */
	tcgetattr(fileno(stdin), &old_flags);
	memcpy(&new_flags, &old_flags, sizeof(old_flags));
	new_flags.c_lflag &= ~ECHO;
	new_flags.c_lflag |= ECHONL;

	if (tcsetattr(fileno(stdin), TCSANOW, &new_flags) != 0) {
		perror("tcsetattr");
		exit(EXIT_FAILURE);
	}

	printf("%s", prompt);
	fgets(passwd, size, stdin);

	/* The result in passwd is '\0' terminated and may contain a final
	 * '\n'. If it exists, we remove it.
	 */
	if (passwd[strlen(passwd) - 1] == '\n') {
		passwd[strlen(passwd) - 1] = '\0';
	}

	/* Restore the terminal */
	if (tcsetattr(fileno(stdin), TCSANOW, &old_flags) != 0) {
		perror("tcsetattr");
		exit(EXIT_FAILURE);
	}
}



/* If someone kills the client, it should still clean up the readline
   library, otherwise the terminal is in a inconsistent state. We set
   active to 0 to get out of the loop below. Also note that the select
   call below may return with -1 and errno set to EINTR. Do not exit
   select with this error. */
void
sigint_handler(int signum)
{
		UNUSED(signum);
        active = 0;

        /* We should not use printf inside of signal handlers, this is not
         * considered safe. We may, however, use write() and fsync(). */
        write(STDOUT_FILENO, "Terminated.\n", 12);
        fsync(STDOUT_FILENO);
}


/* The next two variables are used to access the encrypted stream to
 * the server. The socket file descriptor server_fd is provided for
 * select (if needed), while the encrypted communication should use
 * server_ssl and the SSL API of OpenSSL.
 */
static int server_fd;
static SSL *server_ssl;

/* This variable shall point to the name of the user. The initial value
   is NULL. Set this variable to the username once the user managed to be
   authenticated. */
static char *user;

/* This variable shall point to the name of the chatroom. The initial
   value is NULL (not member of a chat room). Set this variable whenever
   the user changed the chat room successfully. */
static char *chatroom;

/* This prompt is used by the readline library to ask the user for
 * input. It is good style to indicate the name of the user and the
 * chat room he is in as part of the prompt. */
static char *prompt;



/* When a line is entered using the readline library, this function
   gets called to handle the entered line. Implement the code to
   handle the user requests in this function. The client handles the
   server messages in the loop in main(). */
void readline_callback(char *line)
{
        if (NULL == line) {
                rl_callback_handler_remove();
                active = 0;
				g_free(line);
                return;
        }
        if (strlen(line) > 0) {
                add_history(line);
        }
        if ((strncmp("/bye", line, 4) == 0) ||
            (strncmp("/quit", line, 5) == 0)) {
                rl_callback_handler_remove();
                active = 0;
				g_free(line);
                return;
        }
        if (strncmp("/game", line, 5) == 0) {
                /* Skip whitespace */
                int i = 4;
                while (line[i] != '\0' && isspace(line[i])) { i++; }
                if (line[i] == '\0') {
                        write(STDOUT_FILENO, "Usage: /game username\n",
                              29);
                        fsync(STDOUT_FILENO);
                        rl_redisplay();
						g_free(line);
                        return;
                }
                /* Start game */
				g_free(line);
                return;
        }
        if (strncmp("/join", line, 5) == 0) {
                int i = 5;
                /* Skip whitespace */
                while (line[i] != '\0' && isspace(line[i])) { i++; }
                if (line[i] == '\0') {
                        write(STDOUT_FILENO, "Usage: /join chatroom\n", 22);
                        fsync(STDOUT_FILENO);
                        rl_redisplay();
						g_free(line);
                        return;
                }
                char *new_chatroom = strdup(&(line[i]));

                /* Process and send this information to the server. */
				packet_simple *message = g_new0(packet_simple, 1);
				message->opcode = htons(OP_JOIN);
				int n = snprintf(message->buf, SIMPLE_BUF_SIZE - 1,
					"%s", new_chatroom);
                SSL_write(server_ssl, message, 3 + n);
				g_free(message);

                /* Maybe update the prompt. */
                free(prompt);
                prompt = NULL; /* What should the new prompt look like? */
				rl_set_prompt(prompt);
				g_free(new_chatroom);
				g_free(line);
                return;
        }
        if (strncmp("/list", line, 5) == 0) {
				short *message = g_new(short, 1);
				*message = htons(OP_LIST);
				SSL_write(server_ssl, message, 2);
				g_free(message);
				g_free(line);
                return;
        }
        if (strncmp("/roll", line, 5) == 0) {
                /* roll dice and declare winner. */
				g_free(line);
                return;
        }
        if (strncmp("/say", line, 4) == 0) {
                /* Skip whitespace */
                int i = 4;
                while (line[i] != '\0' && isspace(line[i])) { i++; }
                if (line[i] == '\0') {
                        write(STDOUT_FILENO, "Usage: /say username message\n",
                              29);
                        fsync(STDOUT_FILENO);
                        rl_redisplay();
						g_free(line);
                        return;
                }
                /* Skip whitespace */
                int j = i+1;
                while (line[j] != '\0' && isgraph(line[j])) { j++; }
                if (line[j] == '\0') {
                        write(STDOUT_FILENO, "Usage: /say username message\n",
                              29);
                        fsync(STDOUT_FILENO);
                        rl_redisplay();
						g_free(line);
                        return;
                }
                char *receiver = strndup(&(line[i]), j - i);
                char *message = strdup(&(line[j+1]));

                /* Send private message to receiver. */
				packet_simple *msg = g_new0(packet_simple, 1);
				msg->opcode = htons(OP_PRVT);
				int n = sprintf(msg->buf, "%s", receiver);
				n += sprintf(msg->buf+n+1, "%s", message);
				SSL_write(server_ssl, msg, 3 + n);
				g_free(msg);

				free(receiver);
				free(message);
				g_free(line);
                return;
        }
        if (strncmp("/user", line, 5) == 0) {
                int i = 5;
                /* Skip whitespace */
                while (line[i] != '\0' && isspace(line[i])) { i++; }
                if (line[i] == '\0') {
                        write(STDOUT_FILENO, "Usage: /user username\n", 22);
                        fsync(STDOUT_FILENO);
                        rl_redisplay();
						g_free(line);
                        return;
                }
                char *new_user = strdup(&(line[i]));
                char passwd[48];
                getpasswd("Password: ", passwd, 48);

				/* Prepend salt to password */
				char *salt_passwd = malloc(strlen(passwd) + salt_length + 1);
				strcpy(salt_passwd, salt);
				strcat(salt_passwd, passwd);

				/* Hash salted password */
				char *hashed_passwd = g_new0(char, 32);
				SHA256_CTX_ ctx;
				sha256_init(&ctx);
				sha256_update(&ctx, (uchar*)salt_passwd, strlen(salt_passwd));
				sha256_final(&ctx, (uchar*)hashed_passwd);

                /* Process and send this information to the server. */
				packet_simple *message = g_new0(packet_simple, 1);
				message->opcode = htons(OP_AUTH);
				int n = sprintf(message->buf, "%s", new_user) + 1;
				n += sprintf(message->buf + n, "%s", hashed_passwd) + 1;
				SSL_write(server_ssl, message, 2+n);
				free(message);
				free(salt_passwd);
				free(hashed_passwd);
				free(new_user);

                /* Maybe update the prompt. */
                free(prompt);
                prompt = NULL; /* What should the new prompt look like? */
				rl_set_prompt(prompt);
				g_free(line);
                return;
        }
        if (strncmp("/who", line, 4) == 0) {
				short *message = g_new0(short, 1);
				*message = htons(OP_WHO);
                SSL_write(server_ssl, message, 2);
				free(message);
				g_free(line);
                return;
        }

		packet_simple *message = g_new0(packet_simple, 1);
		message->opcode = htons(OP_MSG);
		int n = snprintf(message->buf, SIMPLE_BUF_SIZE - 1, "%s", line);
		SSL_write(server_ssl, message, 3 + n);
		free(message);
		g_free(line);

        /* Sent the buffer to the server. */
        //snprintf(buffer, 255, "Message: %s\n", line);
        //write(STDOUT_FILENO, buffer, strlen(buffer));
        fsync(STDOUT_FILENO);
}

int main(int argc, char **argv)
{
	int err;
	/* Initialize OpenSSL */
	SSL_library_init();
	SSL_load_error_strings();
	SSL_CTX *ssl_ctx = SSL_CTX_new(TLSv1_client_method());
	struct sockaddr_in server_addr;
	struct hostent *he;
	char message[512];

	if (argc < 3) {
		printf("Server address and port is required.\n");
		exit(1);
	}

	get_path_dir();

	/* TODO:
	 * We may want to use a certificate file if we self sign the
	 * certificates using SSL_use_certificate_file(). If available,
	 * a private key can be loaded using
	 * SSL_CTX_use_PrivateKey_file(). The use of private keys with
	 * a server side key data base can be used to authenticate the
	 * client.
	 */
	 if (SSL_CTX_use_certificate_file(ssl_ctx, fullpath_to("client-cert.pem"),
	 	SSL_FILETYPE_PEM) <= 0) {
		 ERR_print_errors_fp(stdout);
		 exit(1);
	 }

	 if (SSL_CTX_use_PrivateKey_file(ssl_ctx, fullpath_to("client-key.pem"),
	 	SSL_FILETYPE_PEM) <= 0) {
		 ERR_print_errors_fp(stdout);
		 exit(1);
	 }

	 if (!SSL_CTX_check_private_key(ssl_ctx)) {
		 printf("Private key and certificate don't match.\n");
		 exit(1);
	 }

	server_ssl = SSL_new(ssl_ctx);

	/* Create and set up a listening socket. The sockets you
	 * create here can be used in select calls, so do not forget
	 * them.
	 */
	server_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	RETURN_ERR(server_fd, "socket");

	if ((he = gethostbyname(argv[1])) == NULL) {
    	exit(1); /* error */
	}

	memset(&server_addr, '\0', sizeof(server_addr));
	server_addr.sin_family      = AF_INET;
	server_addr.sin_port        = htons(strtol(argv[2], NULL, 0));
	memcpy(&server_addr.sin_addr, he->h_addr_list[0], he->h_length);

	err = connect(server_fd, (struct sockaddr*) &server_addr, sizeof(server_addr));
	RETURN_ERR(err, "connect");

	/* Use the socket for the SSL connection. */
	SSL_set_fd(server_ssl, server_fd);

	err = SSL_connect(server_ssl);
	RETURN_SSL(err);
	/* Now we can create BIOs and use them instead of the socket.
	 * The BIO is responsible for maintaining the state of the
	 * encrypted connection and the actual encryption. Reads and
	 * writes to sock_fd will insert unencrypted data into the
	 * stream, which even may crash the server.
	 */

        /* Set up secure connection to the chatd server. */

        /* Read characters from the keyboard while waiting for input.
         */
        prompt = strdup("> ");
        rl_callback_handler_install(prompt, (rl_vcpfunc_t*) &readline_callback);
        while (active) {
                fd_set rfds;
				struct timeval timeout;

                FD_ZERO(&rfds);
                FD_SET(STDIN_FILENO, &rfds);
				FD_SET(server_fd, &rfds);
				int higher = (STDIN_FILENO < server_fd ? server_fd : STDIN_FILENO);
				timeout.tv_sec = 5;
				timeout.tv_usec = 0;

                int r = select(higher + 1, &rfds, NULL, NULL, &timeout);
                if (r < 0) {
                        if (errno == EINTR) {
                                /* This should either retry the call or
                                   exit the loop, depending on whether we
                                   received a SIGTERM. */
                                continue;
                        }
                        /* Not interrupted, maybe nothing we can do? */
                        perror("select()");
                        break;
                }
                if (r == 0) {
						/* I didn't think it makes much sense to print this */
                        //write(STDOUT_FILENO, "No message?\n", 12);
                        //fsync(STDOUT_FILENO);
                        /* Whenever you print out a message, call this
                           to reprint the current input line. */
						rl_redisplay();
                        continue;
                }
				/* Data available on stdin */
                if (FD_ISSET(STDIN_FILENO, &rfds)) {
                        rl_callback_read_char();
						rl_redisplay();
                }
				/* Data available from server */
				if (FD_ISSET(server_fd, &rfds)) {
					int read = SSL_read(server_ssl, message, sizeof(message) - 1);
					message[read] = '\0';
					short *opcode = (short*)message;
					*opcode = ntohs(*opcode);
			        /* A 0 byte packet means the other end disconnected */
					if (read == 0) {
						dprintf(STDOUT_FILENO, "Disconnected from server.\n");
						fsync(STDOUT_FILENO);
						rl_callback_handler_remove();
		                active = 0;
					}
					/* Response to /who and /list */
					else if (*opcode == OP_WHO || *opcode == OP_LIST) {
						packet_simple *res = (packet_simple*)opcode;
						write(STDOUT_FILENO, res->buf, read-2);
						fsync(STDOUT_FILENO);
					}
					/* A public and private message within the chatroom */
					else if (*opcode == OP_MSG || *opcode == OP_PRVT) {
						packet_simple *res = (packet_simple*)opcode;
						char *sender = res->buf;
						char *msg = sender + strlen(sender) + 1;
						dprintf(STDOUT_FILENO,
								(*opcode == OP_PRVT ? "P %s: %s\n" :
								"%s: %s\n"),
								sender, msg);
						fsync(STDOUT_FILENO);
					}
					/* Response to authentication */
					else if (*opcode == OP_AUTH) {
						packet_res *res = (packet_res*)opcode;
						if (ntohs(res->success) == 1) {
							dprintf(STDOUT_FILENO, "Authentication successful.\n");
							fsync(STDOUT_FILENO);
							g_free(user);
							user = strdup(res->buf);
							prompt = g_new0(char, read);
							snprintf(prompt, read, "%s> ", user);
							rl_set_prompt(prompt);
						} else {
							dprintf(STDOUT_FILENO, "Authentication failed.\n");
							dprintf(STDOUT_FILENO, "Reason: %s\n", res->buf);
						}
						fsync(STDOUT_FILENO);
					}
					/* Response to join */
					else if (*opcode == OP_JOIN) {
						packet_res *res = (packet_res*)opcode;
						if (ntohs(res->success) == 1) {
							dprintf(STDOUT_FILENO, "Joined %s\n", res->buf);
							g_free(chatroom);
							chatroom = strdup(res->buf);
						} else {
							dprintf(STDOUT_FILENO, "Joining chatroom failed.\n");
							dprintf(STDOUT_FILENO, "Reason: %s\n", res->buf);
						}
						fsync(STDOUT_FILENO);
					}
					/* Message from server */
					else if (*opcode == OP_SERV) {
						packet_simple *res = (packet_simple*)opcode;
						dprintf(STDOUT_FILENO, "Message from server:\n");
						dprintf(STDOUT_FILENO, "%s\n", res->buf);
						fsync(STDOUT_FILENO);
					}
					/* Error from server */
					else if (*opcode == OP_ERR) {
						packet_simple *res = (packet_simple*)opcode;
						dprintf(STDOUT_FILENO, "Server error:\n");
						dprintf(STDOUT_FILENO, "%s\n", res->buf);
						fsync(STDOUT_FILENO);
					}
					rl_redisplay();
				}
        }
        /* replace by code to shutdown the connection and exit
           the program. */
		SSL_shutdown(server_ssl);
		close(server_fd);
		SSL_free(server_ssl);
		SSL_CTX_free(ssl_ctx);
		/* g_free is NULL-safe */
		g_free(chatroom);
		g_free(user);
		g_free(prompt);
}

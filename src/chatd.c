/* A TCP echo server with timeouts.
 *
 * Note that you will not need to use select and the timeout for a
 * tftp server. However, select is also useful if you want to receive
 * from multiple sockets at the same time. Read the documentation for
 * select on how to do this (Hint: Iterate with FD_ISSET()).
 */

#include <assert.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <glib.h>

#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "connection.h"
#include "packet_types.h"
#include "fullpath.h"

/* To suppress unused parameter warning */
#define UNUSED(x) (void)(x)
#define MESSAGE_SIZE 512

#define TIMEOUT  5
#define MAX_IDLE 1800 /* 30 minutes */

int highest_fd;
GTree* connections;
GTree* chatrooms;
GTree* users;
GSList* disconnect_list = NULL;

typedef struct msg_container {
    int size;
    char *msg;
} msg_container;

typedef struct conn_check {
    fd_set *rfds;
    struct timeval *tv;
} conn_check;

int user_list(gpointer key, gpointer value, gpointer data) {
    struct sockaddr_in *client = (struct sockaddr_in*)key;
    connection *conn = (connection*)value;
    if (conn->chatroom != NULL && strlen(conn->username) > 0){
        int length = strlen(data);
        sprintf(data + length, "%s (%s:%d) - %s\n", conn->username,
            inet_ntoa(client->sin_addr), ntohs(client->sin_port), conn->chatroom);
    }
    return 0;
}

int chatroom_list(gpointer key, gpointer value, gpointer data) {
    UNUSED(value);
    int length = strlen(data);
    sprintf(data + length, "%s\n", (char*)key);
    return 0;
}

int send_to_all(gpointer data, gpointer user_data) {
    connection *conn = (connection*)data;
    msg_container *container = (msg_container*)user_data;
    SSL_write(conn->ssl, container->msg, container->size);
    return 0;
}

void log_connection(struct sockaddr_in *client, char *state) {
    time_t now;
    time(&now);
    char timestring[sizeof "YYYY-MM-DDTHH:MM:SSZ"];

    strftime(timestring, sizeof timestring, "%FT%TZ", gmtime(&now));
    FILE* f = fopen(fullpath_to("chatd.log"), "a");
    fprintf(f, "%s : %s:%d %s\n", timestring,
        inet_ntoa(client->sin_addr), ntohs(client->sin_port), state);
    fclose(f);
}

void log_authentication(struct sockaddr_in *client, char* username, char *state) {
    time_t now;
    time(&now);
    char timestring[sizeof "YYYY-MM-DDTHH:MM:SSZ"];

    strftime(timestring, sizeof timestring, "%FT%TZ", gmtime(&now));
    FILE* f = fopen(fullpath_to("chatd.log"), "a");
    fprintf(f, "%s : %s:%d %s %s\n", timestring,
        inet_ntoa(client->sin_addr), ntohs(client->sin_port), username, state);
    fclose(f);
}

int authenticate_user(char *username, char *passwd) {
    int ret = 0;
    int fd = open(fullpath_to("passwords.ini"), O_RDWR | O_CREAT,
        S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    char *passwd64 = NULL, *new_passwd64 = NULL;
    gsize length;
    GKeyFile *keyfile = g_key_file_new();
    g_key_file_load_from_file(keyfile, fullpath_to("passwords.ini"),
        G_KEY_FILE_NONE, NULL);

    passwd64 = g_key_file_get_string(keyfile, "passwords",
                            username, NULL);
    new_passwd64 = g_base64_encode((guchar*)passwd, strlen(passwd));

    if (passwd64 != NULL) {
        if (strcmp(passwd64, new_passwd64) == 0) {
            ret = 1;
        } else {
            ret = 0;
        }
    } else {
        g_key_file_set_string(keyfile, "passwords", username, new_passwd64);
        gchar *keyfile_string = g_key_file_to_data(keyfile, &length, NULL);
        write(fd, keyfile_string, length);
        g_free(keyfile_string);
        ret = 1;
    }
    g_free(passwd64);
    g_free(new_passwd64);
    g_key_file_free(keyfile);
    close(fd);
    return ret;
}

void handle_message(struct sockaddr_in *client, connection *conn, char *msg) {
    UNUSED(client);
    short *opcode = (short*)msg;
    *opcode = ntohs(*opcode);
    char *buffer = g_new0(char, MSG_SIZE);
    int is_auth = strlen(conn->username) > 0;

    /* Every time a client sends a command we reset the idle timer */
    conn->idle = 0;

    /* Client tries to authenticate */
    if (*opcode == OP_AUTH) {
        char *username = (char*)opcode+2; int size = strlen(username) + 1;
        char *passwd   = username + size;
        packet_res *response = (packet_res*)buffer;
        response->opcode = htons(OP_AUTH);
        if (authenticate_user(username, passwd)) {
            void *old_conn = g_tree_lookup(users, username);
            if (old_conn == NULL) {
                if (strlen(conn->username) > 0) {
                    /* Connection is already authenticated as someone else
                     * and wants to change user */
                    g_tree_remove(users, conn->username);
                }
                snprintf(conn->username, size, "%s", username);
                g_tree_insert(users, conn->username, conn);
                response->success = htons(1);
                size = snprintf(response->buf, RES_BUF_SIZE-1, "%s", username) + 5;
                log_authentication(client, username, "authenticated");
                /* Reset auth_tries counter */
                conn->auth_tries = 0;
            } else {
                response->success = htons(0);
                size = sprintf(response->buf, "User already logged in.") + 5;
                log_authentication(client, username, "authentication error");
                conn->auth_tries += 1;
            }
        } else {
            response->success = htons(0);
            size = sprintf(response->buf, "Password doesn't match.") + 5;
            log_authentication(client, username, "authentication error");
            conn->auth_tries += 1;
        }
        SSL_write(conn->ssl, buffer, size);
    }
    /* Client sent the /who command */
    else if (*opcode == OP_WHO) {
        packet_simple *response = (packet_simple*)buffer;
        response->opcode = htons(OP_WHO);
        g_tree_foreach(connections, user_list, response->buf);
        SSL_write(conn->ssl, response, 3+strlen(response->buf));
    }
    /* Client sent the /list command */
    else if (*opcode == OP_LIST) {
        packet_simple *response = (packet_simple*)buffer;
        response->opcode = htons(OP_LIST);
        g_tree_foreach(chatrooms, chatroom_list, response->buf);
        SSL_write(conn->ssl, response, 3+strlen(response->buf));
    }

    /* Commands below should not work unless user is authenticated */

    /* Client wants to join a channel */
    else if (*opcode == OP_JOIN) {
        int size = 0;
        packet_res *response = (packet_res*)buffer;
        response->opcode = htons(OP_JOIN);
        if (is_auth) {
            GList *userlist = NULL;
            char *chatroom = NULL;
            /* I use extended lookup because an empty GList is just a NULL pointer
             * and g_tree_lookup returns NULL if nothing is found. So there would
             * be no way of knowing if nothing was found or if an empty list was
             * returned */
            gboolean found = g_tree_lookup_extended(chatrooms, (char*)opcode+2,
                                        (gpointer)&chatroom, (gpointer)&userlist);

            if (conn->chatroom != NULL) {
                /* Removing the user from the old chatroom */
                GList *old_chan_list = g_tree_lookup(chatrooms, conn->chatroom);
                old_chan_list = g_list_remove(old_chan_list, conn);
                g_tree_insert(chatrooms, conn->chatroom, old_chan_list);
            }

            /* Adding user to the new chatroom */
            conn->chatroom = found ? chatroom : strdup((char*)opcode+2);
            userlist = g_list_prepend(userlist, conn);
            g_tree_insert(chatrooms, conn->chatroom, userlist);
            /* Making a response */
            response->success = htons(1);
            size = snprintf(response->buf, RES_BUF_SIZE-1, "%s", conn->chatroom) + 5;
        }
        /* Not authenticated */
        else {
            response->success = htons(0);
            size = snprintf(response->buf, RES_BUF_SIZE-1, "Not authenticated.") + 5;
        }
        SSL_write(conn->ssl, response, size);
    }
    /* A public message from client */
    else if (*opcode == OP_MSG) {
        packet_simple *response = (packet_simple*)buffer;
        response->opcode = htons(OP_MSG);
        if (is_auth) {
            if (conn->chatroom != NULL) {
                /* Add username to buffer */
                int n = snprintf(response->buf, SIMPLE_BUF_SIZE-1, "%s", conn->username) + 1;
                /* Find end of username */
                char *msg = response->buf + n;
                /* Add message right after username */
                n += snprintf(msg, SIMPLE_BUF_SIZE-n-1, "%s", (char*)opcode+2) + 1;
                /* Add the packet to container */
                msg_container container;
                container.size = 2 + n;
                container.msg = buffer;
                /* Send to all client in the same chatroom */
                GList *userlist = g_tree_lookup(chatrooms, conn->chatroom);
                g_list_foreach(userlist, (GFunc)send_to_all, &container);
            }
            /* Not in chatroom */
            else {
                response->opcode = htons(OP_ERR);
                int n = snprintf(response->buf, SIMPLE_BUF_SIZE-1,
                    "You are not in any chatroom.") + 1;
                SSL_write(conn->ssl, response, 2+n);
            }
        }
        /* Not authenticated */
        else {
            response->opcode = htons(OP_ERR);
            int n = snprintf(response->buf, SIMPLE_BUF_SIZE-1,
                "Not authenticated.") + 1;
            SSL_write(conn->ssl, response, 2+n);
        }
    }
    /* A private message from client */
    else if (*opcode == OP_PRVT) {
        packet_simple *response = (packet_simple*)buffer;
        response->opcode = htons(OP_PRVT);
        if (is_auth) {
            if (conn->chatroom != NULL) {
                char *destination = (char*)opcode+2;
                char *message = destination + strlen(destination) + 1;
                /* Find the correct user */
                connection *user_conn = g_tree_lookup(users, destination);
                if (user_conn != NULL) {
                    /* Add username to buffer */
                    int n = snprintf(response->buf, SIMPLE_BUF_SIZE-1, "%s", conn->username) + 1;
                    /* Find end of username */
                    char *msg = response->buf + n;
                    /* Add message right after username */
                    n += snprintf(msg, SIMPLE_BUF_SIZE-n-1, "%s", message) + 1;
                    /* Send the message to the user */
                    SSL_write(user_conn->ssl, buffer, 2+n);
                }
                /* Destination user was not found */
                else {
                    response->opcode = htons(OP_ERR);
                    int n = snprintf(response->buf, SIMPLE_BUF_SIZE-1,
                        "User not found.") + 1;
                    SSL_write(conn->ssl, response, 2+n);
                }
            }
            /* Not in chatroom */
            else {
                response->opcode = htons(OP_ERR);
                int n = snprintf(response->buf, SIMPLE_BUF_SIZE-1,
                    "You are not in any chatroom.") + 1;
                SSL_write(conn->ssl, response, 2+n);
            }
        }
        /* Not authenticated */
        else {
            response->opcode = htons(OP_ERR);
            int n = snprintf(response->buf, SIMPLE_BUF_SIZE-1,
                "Not authenticated.") + 1;
            SSL_write(conn->ssl, response, 2+n);
        }
    }
    g_free(buffer);
}

int conn_tree_insert_set(gpointer key, gpointer value, gpointer data) {
    UNUSED(key);
    FD_SET(((connection*)value)->sockfd, (fd_set*)data);
    highest_fd = (((connection*)value)->sockfd > highest_fd ?
                    ((connection*)value)->sockfd : highest_fd);
    return 0;
}

int conn_tree_check(gpointer key, gpointer value, gpointer data) {
    if (key == NULL || value == NULL) return 0;
    char *message = g_new0(char, MESSAGE_SIZE);
    struct sockaddr_in *client = (struct sockaddr_in*) key;
    struct connection *conn = (connection*) value;
    conn_check *envelope = (conn_check*)data;
    conn->idle += TIMEOUT - envelope->tv->tv_sec;
    int n = 0;
    if (FD_ISSET(conn->sockfd, envelope->rfds)) {
        if ((n = SSL_read(conn->ssl, message, MESSAGE_SIZE - 1)) != 0) {
            message[n] = '\0';
            handle_message(client, conn, message);
            conn->idle = 0;
        }
        /* A 0 byte packet means the other end disconnected */
        else {
            /* The glib documentation says you can't modify the tree
             * while iterating through it. Therefor I add the key to
             * a list to later iterate through it and delete them */
            disconnect_list = g_slist_prepend(disconnect_list, client);
            log_connection(client, "disconnected");
        }
    }

    if (conn->idle > MAX_IDLE || conn->auth_tries > 2) {
        /* The glib documentation says you can't modify the tree
         * while iterating through it. Therefor I add the key to
         * a list to later iterate through it and delete them */
        disconnect_list = g_slist_prepend(disconnect_list, client);
        log_connection(client, "disconnected");
    }
    free(message);
    return 0;
}

void disconnect_conn(gpointer data, gpointer user_data) {
    UNUSED(user_data);
    connection *conn = g_tree_lookup(connections, data);
    if (strlen(conn->username) > 0) {
        g_tree_remove(users, conn->username);
    }
    if (((connection*)conn)->chatroom != NULL) {
        GList *list = g_tree_lookup(chatrooms, ((connection*)conn)->chatroom);
        list = g_list_remove(list, (connection*)conn);
        g_tree_insert(chatrooms, ((connection*)conn)->chatroom, list);
    }
    g_tree_remove(connections, data);
}

/* This can be used to build instances of GTree that index on
   the address of a connection. */
int sockaddr_in_cmp(const void *addr1, const void *addr2)
{
        const struct sockaddr_in *_addr1 = addr1;
        const struct sockaddr_in *_addr2 = addr2;

        /* If either of the pointers is NULL or the addresses
           belong to different families, we abort. */
        g_assert((_addr1 != NULL) || (_addr2 != NULL) ||
                 (_addr1->sin_family != _addr2->sin_family));

        if (_addr1->sin_addr.s_addr < _addr2->sin_addr.s_addr) {
                return -1;
        } else if (_addr1->sin_addr.s_addr > _addr2->sin_addr.s_addr) {
                return 1;
        } else if (_addr1->sin_port < _addr2->sin_port) {
                return -1;
        } else if (_addr1->sin_port > _addr2->sin_port) {
                return 1;
        }
        return 0;
}

int main(int argc, char **argv)
{
        int sockfd;
        struct sockaddr_in server, client;
        struct sockaddr_in *key;
        connection *value;

        if (argc < 2) {
            printf("Port number missing.\n");
            exit(1);
        }

        get_path_dir();

        const SSL_METHOD *meth = TLSv1_method();

        SSL_CTX    *ctx;
        SSL        *ssl;

        SSL_library_init();       /* encryption and hash algorithms for SSL */
        SSL_load_error_strings(); /* error strings for good error reporting */

        ctx  = SSL_CTX_new(meth);


        if (SSL_CTX_use_certificate_file(ctx, fullpath_to("server-cert.pem"),
                SSL_FILETYPE_PEM) <= 0) {
            ERR_print_errors_fp(stdout);
            exit(1);
        }

        if (SSL_CTX_use_PrivateKey_file(ctx, fullpath_to("server-key.pem"),
            SSL_FILETYPE_PEM) <= 0) {
            ERR_print_errors_fp(stdout);
            exit(1);
        }

        if (!SSL_CTX_check_private_key(ctx)) {
            printf("Private key and certificate don't match.\n");
            exit(1);
        }

        /* Create and bind a TCP socket */
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        memset(&server, 0, sizeof(server));
        server.sin_family = AF_INET;
        /* Network functions need arguments in network byte order instead of
           host byte order. The macros htonl, htons convert the values, */
        server.sin_addr.s_addr = htonl(INADDR_ANY);
        server.sin_port = htons(strtol(argv[1], NULL, 0));
        if (bind(sockfd, (struct sockaddr *) &server, (socklen_t) sizeof(server)) == -1) {
            printf("Bind error\n");
            exit(1);
        }

        connections = g_tree_new_full((GCompareDataFunc)sockaddr_in_cmp, NULL,
                                free, connection_free);
        chatrooms   = g_tree_new((GCompareFunc)strcmp);
        users       = g_tree_new((GCompareFunc)strcmp);

        /* Hardcode available chatrooms */
        g_tree_insert(chatrooms, "Linux", NULL);
        g_tree_insert(chatrooms, "Cats", NULL);
        g_tree_insert(chatrooms, "Car lovers", NULL);

	/* Before we can accept messages, we have to listen to the port. We allow one
	 * 1 connection to queue for simplicity.
	 */
	listen(sockfd, 10);

        for (;;) {
                fd_set rfds;
                struct timeval tv;
                int retval;

                /* Check whether there is data on the socket fd. */
                FD_ZERO(&rfds);
                FD_SET(sockfd, &rfds);
                highest_fd = sockfd;

                g_tree_foreach(connections, (GTraverseFunc)conn_tree_insert_set, &rfds);

                /* Wait for five seconds. */
                tv.tv_sec = TIMEOUT;
                tv.tv_usec = 0;
                retval = select(highest_fd + 1, &rfds, NULL, NULL, &tv);

                if (retval == -1) {
                        perror("select()");
                } else if (retval > 0) {
                        /* New connection incoming */
                        if (FD_ISSET(sockfd, &rfds)) {
                            /* Copy to len, since recvfrom may change it. */
                            socklen_t len = (socklen_t) sizeof(client);

                            /* For TCP connectios, we first have to accept. */
                            int connfd;
                            connfd = accept(sockfd, (struct sockaddr *) &client,
                                            &len);

                            /* Create a SSL structure */
                            ssl = SSL_new(ctx);

                            /* Accept SSL connection */
                            SSL_set_fd(ssl, connfd);
                            SSL_accept(ssl);

                            /* Add connection to tree */
                            key = malloc(sizeof(struct sockaddr_in));
                            memcpy(key, &client, sizeof(client));

                            value = connection_new(ssl, connfd);
                            g_tree_insert(connections, key, value);
                            log_connection(&client, "connected");

                            packet_simple *msg = g_new0(packet_simple, 1);
                            msg->opcode = htons(OP_SERV);
                            int n = snprintf(msg->buf, SIMPLE_BUF_SIZE-1, "Welcome.") + 1;
                            SSL_write(ssl, msg, 2+n);
                            free(msg);
                        }
                } else {
                        fprintf(stdout, "No message in five seconds.\n");
                        fflush(stdout);
                }

                /* Check for incoming data as well as updating the idle timer */
                conn_check envelope;
                envelope.rfds = &rfds;
                envelope.tv = &tv;
                g_tree_foreach(connections,
                    (GTraverseFunc)conn_tree_check, &envelope);

                /* disconnect connections */
                if (disconnect_list != NULL) {
                    g_slist_foreach(disconnect_list, disconnect_conn, NULL);
                    g_slist_free(disconnect_list);
                    disconnect_list = NULL;
                }
        }
}

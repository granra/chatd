char exedir[200];
char pathtmp[300];

void get_path_dir() {
    int n = readlink("/proc/self/exe", exedir, 200);
    char *end = exedir + n;
    for (; *end != '/' && end > exedir; --end);
    *(end+1) = '\0';
}

char *fullpath_to(char *str) {
    strcpy(pathtmp, exedir);
    strcat(pathtmp, str);
    return pathtmp;
}
